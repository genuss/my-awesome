#!/usr/bin/env sh

set -x

echo $APP_LOCATION

MAIN_CLASS=$(grep 'Start-Class' $APP_LOCATION/META-INF/MANIFEST.MF | awk '{print $2}')

echo "Main class is $MAIN_CLASS"

java $JAVA_OPTS -cp $APP_LOCATION/BOOT-INF/classes:$APP_LOCATION/BOOT-INF/lib/*:$APP_LOCATION/BOOT-INF $MAIN_CLASS
