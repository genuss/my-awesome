package me.genuss.project1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class Project1Application {

  public static void main(String[] args) {
    SpringApplication.run(Project1Application.class, args);
  }

}

@RestController
class HelloController {

  @RequestMapping("/")
  public String hello() {
    return "Hello";
  }
}
