#!/usr/bin/env sh

apk add curl

set -ex

env

echo preparing ssh-key

mkdir -p ~/.ssh
chmod 700 ~/.ssh
cat $CI_REPO_DEPLOY_KEY > ~/.ssh/id_rsa
chmod 400 ~/.ssh/id_rsa

ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts


echo Tagging release
VERSION=$(date +%F)-$CI_PIPELINE_ID

docker login -u gitlab-ci-runner -p $CI_REGISTRY_TOKEN $CI_REGISTRY
docker pull $CI_REGISTRY_IMAGE/$PROJECT_NAME:$CI_COMMIT_SHORT_SHA
docker tag $CI_REGISTRY_IMAGE/$PROJECT_NAME:$CI_COMMIT_SHORT_SHA $CI_REGISTRY_IMAGE/$PROJECT_NAME:$VERSION
docker push $CI_REGISTRY_IMAGE/$PROJECT_NAME:$VERSION


echo searching for latest release
LATEST_TAG_ID=$(git for-each-ref refs/tags --sort=-taggerdate | grep project2 | head -n1 | awk '{print $1}')
TEN_COMMIT_BACK_ID=$(git rev-parse HEAD~10)

if [ -z "$LATEST_TAG_ID" ]; then
  LATEST_TAG_ID=$TEN_COMMIT_BACK_ID
fi

echo Pushing tag to git
git config --global user.email "gitlab@genuss.me"
git config --global user.name "Gitlab Tag Master"
TAG="$PROJECT_NAME-$VERSION"
git tag --annotate --message "Tagging release of $PROJECT_NAME from CI. Version $VERSION" $TAG
git push "git@gitlab.com:$CI_PROJECT_PATH.git" $TAG HEAD:$CI_COMMIT_BRANCH

echo "deployed $PROJECT_NAME with version $VERSION"

echo creating release

CHANGES=$(git log --pretty=tformat:'%s%x0D%x0A' $LATEST_TAG_ID..HEAD $PROJECT_NAME)
cat > release_notes <<EOT
{
  "name": "Release of $PROJECT_NAME $VERSION",
  "tag_name": "$TAG",
  "description": "Super nice release\r\n\r\nChanges:\r\n\r\n$CHANGES\r\n\r\n\r\nDocker image: $CI_REGISTRY_IMAGE/$PROJECT_NAME:$VERSION"
}
EOT

curl -v \
  --header 'Content-Type: application/json' \
  --header "PRIVATE-TOKEN: $CI_REGISTRY_TOKEN" \
  --data @release_notes \
  --request POST $CI_API_V4_URL/projects/$CI_PROJECT_ID/releases
