#!/usr/bin/env sh

set -ex

cp common-lib/Dockerfile \
  common-lib/entrypoint.sh \
  $PROJECT_NAME/build/tmp

docker build \
  -t $CI_REGISTRY_IMAGE/$PROJECT_NAME:$CI_COMMIT_SHORT_SHA \
  -f $PROJECT_NAME/build/tmp/Dockerfile \
  $PROJECT_NAME

docker login -u gitlab-ci-runner -p $CI_REGISTRY_TOKEN $CI_REGISTRY

docker push $CI_REGISTRY_IMAGE/$PROJECT_NAME:$CI_COMMIT_SHORT_SHA
