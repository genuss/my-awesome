#!/usr/bin/env sh

set -ex

env

cd $PROJECT_NAME
gradle build --no-daemon

ls -lh . build/libs
